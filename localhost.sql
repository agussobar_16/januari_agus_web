-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 24 Jan 2019 pada 22.41
-- Versi Server: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shirobyte`
--
CREATE DATABASE IF NOT EXISTS `shirobyte` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `shirobyte`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) UNSIGNED NOT NULL,
  `pekerjaan` varchar(50) NOT NULL DEFAULT 'Belum Bekerja',
  `nama` varchar(100) DEFAULT NULL,
  `telp` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `gender` enum('L','P') DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `is_aktif` enum('t','f') DEFAULT 't'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `pekerjaan`, `nama`, `telp`, `email`, `password`, `gender`, `gambar`, `is_aktif`) VALUES
(1, 'Belum Bekerja', 'agus sobari', '089651545235', 'agussobari.16@gmail.com', '202cb962ac59075b964b07152d234b70', 'L', NULL, 't'),
(3, 'Belum Bekerja', 'siapa saja', '0897681111', 'perempuan@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'P', NULL, 't'),
(4, 'Pegawai Negeri', 'perempuan lagi', '089651545235', 'perempuan@gmail.com', '202cb962ac59075b964b07152d234b70', 'P', NULL, 'f'),
(5, 'Karyawan Swasta', 'agus sobari', '111111111111', 'agussobari.16@gmail.com', '202cb962ac59075b964b07152d234b70', 'L', NULL, 't');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
