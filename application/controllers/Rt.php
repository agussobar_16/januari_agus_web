<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rt extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Rt_model', 'rt');
	}

	function index()
	{		
		$data['rw'] = $this->rt->getDatanya();		
		//echo '<pre>';
		//var_dump($data['rw']);
		//echo '</pre>';
		$this->load->view('rt', $data);
	}
}
