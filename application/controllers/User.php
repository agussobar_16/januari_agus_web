<?php
// application/controllers/Validation.php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class User extends CI_Controller {
    public function __construct() 
    {
      parent::__construct();
             
      $this->load->helper(array('form', 'url'));              
      $this->load->library('form_validation');
      $this->load->library('session');
      $this->load->database();
    }
     
    public function register()
    {
    
    	$data['ket'] = 'Registrasi User'; 
                 
        $this->form_validation->set_rules('nama', 'Nama', 'required|alpha_with_space', 
        array('required' => 'Kolom Nama Harus diisi', 'alpha_with_space' => 'Huruf dan angka saja yang diperbolehkan'));
                
        $this->form_validation->set_rules('telp', 'Nomer Telpon', 'required|is_natural|min_length[8]',
        array('required' => 'Kolom Nomor Telpon Harus diisi', 'is_natural' => 'Hanya Angka yang diperbolehkan', 'min_length' => 'Minimal 8 angka'));
                 
        $this->form_validation->set_rules('email', 'Email Field', 'required|valid_email',
        array('required' => 'Kolom Email Harus diisi', 'valid_email' => 'Gunakan Alamat Email yang valid'));
                 
        $this->form_validation->set_rules('password', 'Password One', 'required',
        array('required' => 'Kolom Password Harus diisi'));
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation Field', 'required|matches[password]',
        array('required' => 'Kolom Konfirmasi Password Harus diisi', 'matches' => 'password tidak sesuai'));
         
        $this->form_validation->set_rules('gender', 'Gender', 'required', 
        array('required' => 'Gender Harus Dipilih')); 
        
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required', 
        array('required' => 'Pekerjaan Harus Dipilih'));           
        
        $this->form_validation->set_rules('userfile', 'Cek', 'required');
         
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('validate_form', $data);
        }
        else
        {
            // load success template...
            //var_dump($this->input->post());
            //die(var_dump($_FILES));
            $this->_act_register();
            $this->session->set_flashdata('msg', 'Data Berhasil Ditambah');
            //$this->load->view('validate_form');
            redirect('user/register');
        }
    }
    
    function edit_user($id_edit)
    {
    	
    	$data['data'] = $this->db->get_where('user', array('id' => $id_edit))->result_array();
    
    	$this->form_validation->set_rules('nama', 'Nama', 'required|alpha_with_space', 
        array('required' => 'Kolom Nama Harus diisi', 'alpha_with_space' => 'Huruf dan angka saja yang diperbolehkan'));
                
        $this->form_validation->set_rules('telp', 'Nomer Telpon', 'required|is_natural|min_length[8]',
        array('required' => 'Kolom Nomor Telpon Harus diisi', 'is_natural' => 'Hanya Angka yang diperbolehkan', 'min_length' => 'Minimal 8 angka'));
                 
        $this->form_validation->set_rules('email', 'Email Field', 'required|valid_email',
        array('required' => 'Kolom Email Harus diisi', 'valid_email' => 'Gunakan Alamat Email yang valid'));
                 
        $this->form_validation->set_rules('password', 'Password One', 'required',
        array('required' => 'Kolom Password Harus diisi'));
        $this->form_validation->set_rules('password_confirmation', 'Password Confirmation Field', 'required|matches[password]',
        array('required' => 'Kolom Konfirmasi Password Harus diisi', 'matches' => 'password tidak sesuai'));
         
        $this->form_validation->set_rules('gender', 'Gender', 'required', 
        array('required' => 'Gender Harus Dipilih')); 
        
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required', 
        array('required' => 'Pekerjaan Harus Dipilih'));       
        
        $this->form_validation->set_rules('userfile', 'Cek', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('validate_form_edit', $data);
        }
        else
        {
            // load success template...
            //var_dump($this->input->post());
            //die(var_dump($_FILES));
            $this->_act_edit($id_edit);
            $this->session->set_flashdata('msg', 'Data Berhasil dirubah');
            //$this->load->view('validate_form');
            redirect('user/list_user');
        }
    }
    
    public function _act_edit($id_edit)
    {
    	$config['upload_path'] = './items_images';
        $config['max_size'] = 1024 * 10;
        $config['allowed_types'] = 'gif|png|jpg|jpeg';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
		
        if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name']))
        {
            if($this->upload->do_upload('userfile'))
            {
                $upload_data = $this->upload->data();
                $gambar = $upload_data['file_name'];
                //return TRUE;
            }
            else
            {
                //$this->form_validation->set_message('upload_image', $this->upload->display_errors());
                //return FALSE;
            }
        }
        else
        {
            $gambar = NULL;
            //return FALSE;
        }
        
    	$data = array(
    		'nama' 			=> $this->input->post('nama'),
    		'telp' 			=> $this->input->post('telp'),
    		'email'		 	=> $this->input->post('email'),
    		'password' 		=> md5($this->input->post('password')),
    		'gender'		=> $this->input->post('gender'),
    		'pekerjaan'		=> $this->input->post('pekerjaan'),
    		'gambar'		=> $gambar    	
    	);
    	
    	$this->db->where('id', $id_edit);
    	$this->db->update('user', $data);
    }
    
    public function list_user()
    {
    	$data['list_user'] = $this->db->get('user')->result_array();
    	$this->load->view('list_user', $data);
    }
    
     public function upload_image()
        {
            $config['upload_path'] = './items_images';
            $config['max_size'] = 1024 * 10;
            $config['allowed_types'] = 'gif|png|jpg|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);

            if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name']))
            {
                if($this->upload->do_upload('userfile'))
                {
                    $upload_data = $this->upload->data();
                    $_POST['userfile'] = $upload_data['file_name'];
                    return TRUE;
                }
                else
                {
                    $this->form_validation->set_message('upload_image', $this->upload->display_errors());
                    return FALSE;
                }
            }
            else
            {
                $_POST['item_img'] = NULL;
                return FALSE;
            }
        }
    
    private function _act_register()
    {
    	$config['upload_path'] = './items_images';
        $config['max_size'] = 1024 * 10;
        $config['allowed_types'] = 'gif|png|jpg|jpeg';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
		
        if(isset($_FILES['userfile']) && !empty($_FILES['userfile']['name']))
        {
            if($this->upload->do_upload('userfile'))
            {
                $upload_data = $this->upload->data();
                $gambar = $upload_data['file_name'];
                //return TRUE;
            }
            else
            {
                //$this->form_validation->set_message('upload_image', $this->upload->display_errors());
                //return FALSE;
            }
        }
        else
        {
            $gambar = NULL;
            //return FALSE;
        }
        
    	$data = array(
    		'nama' 			=> $this->input->post('nama'),
    		'telp' 			=> $this->input->post('telp'),
    		'email'		 	=> $this->input->post('email'),
    		'password' 		=> md5($this->input->post('password')),
    		'gender'		=> $this->input->post('gender'),
    		'pekerjaan'		=> $this->input->post('pekerjaan'),
    		'gambar'		=> $gambar    	
    	);
    	
    	$this->db->insert('user', $data);
    	
    }
    
    function hapus_user($id = NULL)
    {
    	if(!$id)
    	redirect('user/list_user');
    	
    	$this->db->where('id', $id);
    	$this->db->delete('user');
    	
    	redirect('user/list_user');
    }
    
    function ubah_status($id = NULL, $kode = NULL)
    {
    	if(!$id || !$kode)
    	redirect('user/list_user');
    	
    	$status[1] = 'f';
    	$status[2] = 't';
    	
    	$this->db->where('id', $id);
    	$this->db->update('user', array('is_aktif' => $status[$kode]));
    	redirect('user/list_user');
    }
     
}
