<html>
    <head>
    <title>Registrasi User</title>
    <style>
    .field_title{font-size: 13px;font-family:Arial;width: 300px;margin-top: 10px}
    .form_error{font-size: 13px;font-family:Arial;color:red;font-style:italic}
    </style>
    </head>
     
    <body>        
         
        <?php echo form_open(); ?>
         
            <h2><?= $ket?></h2>                               
            <a href = "<?= base_url()?>index.php/user/list_user">List User</a>
			<div>
				<div class="field_title">Email</div>
				<input type="text" name="email" value="<?php echo set_value('email'); ?>" size="30" />
				<?php echo form_error('email', '<div class="form_error">', '</div>'); ?>
			</div> 
			
			<div>
                <div class="field_title">Password</div>
                <input type="password" name="password" value="" size="30" />
                <?php echo form_error('password', '<div class="form_error">', '</div>'); ?>
            </div>
             
            <div>
                <div class="field_title">Password Confirmation</div>
                <input type="password" name="password_confirmation" value="" size="30" />
                <?php echo form_error('password_confirmation', '<div class="form_error">', '</div>'); ?>
            </div>
            
            <div>
                <div class="field_title">Nama</div>
                <input type="text" name="nama" value="<?php echo set_value('nama'); ?>" size="30" />
                <?php echo form_error('nama', '<div class="form_error">', '</div>'); ?>
            </div>
            
            <div>
                <div class="field_title">Gender</div>
                <input type="radio" name="gender" value = "L" <?php echo set_value('gender') == 'L' ? 'checked' : '' ?>> Laki Laki <br>
                <input type="radio" name="gender" value = "P" <?php echo set_value('gender') == 'P' ? 'checked' : '' ?>> Perempuan
                <?php echo form_error('gender', '<div class="form_error">', '</div>'); ?>
            </div>
            
            <div>
                <div class="field_title">No Telpon</div>
                <input type="text" name="telp" value="<?php echo set_value('telp'); ?>" size="30" />
                <?php echo form_error('telp', '<div class="form_error">', '</div>'); ?>
            </div>            
            <div>
                <div class="field_title">Pekerjaan</div>
                <select name = "pekerjaan">
                	<option value = "" disable selected>--Pilih Pekerjaan--</option>
                	<option value = "Karyawan Swasta" <?php echo set_value('pekerjaan') == 'Karyawan Swasta' ? 'selected' : '' ?>>Karyawan Swasta</option>
                	<option value = "Pegawai Negeri" <?php echo set_value('pekerjaan') == 'Pegawai Negeri' ? 'selected' : '' ?>>pegawai negeri</option>
                	<option value = "Belum Bekerja" <?php echo set_value('pekerjaan') == 'Belum Bekerja' ? 'selected' : '' ?>>Belum Bekerja</option>                	                	
                </select>                
                <?php echo form_error('pekerjaan', '<div class="form_error">', '</div>'); ?>
            </div>                        
            <div>
                <div class="field_title">Photo</div>
                <input type="file" name="userfile"/>
                <?php echo form_error('userfile', '<div class="form_error">', '</div>'); ?>
            </div>                  
                                                              
            <div class="field_title">
                <input type="submit" value="Submit" />
            </div>
         
        </form>
        <?php if($this->session->flashdata('msg')){ ?>
		    <div class="form_error">
			<?php echo $this->session->flashdata('msg'); ?>
			</div>
		<?php } ?>
    </body>
</html>
