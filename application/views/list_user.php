<html>
<head>
	<title>List Users</title>
</head>
<body>
<link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/datatables.min.css"/>
<?php ?>
<a href = "<?= base_url()?>index.php/user/register">Tambah User</a>
<?php if($this->session->flashdata('msg')){ ?>
		    <div class="form_error">
			<?php echo $this->session->flashdata('msg'); ?>
			</div>
		<?php } ?>
<table id = "cek" class="display" cellspacing="0" width="100%">
	<thead>
	<tr>
		<td>Nama</td>
		<td>Telp</td>
		<td>Email</td>
		<td>Gender</td>	
		<td>Pekerjaan</td>	
		<td>Action</td>
	</tr>
	</thead>
	<tbody>
		<?php 
			foreach($list_user as $v)
			{
				$status = $v['is_aktif'] == 't' ? '<input type = "button" value = "Non Aktif" onclick = "nonaktifkan('.$v['id'].', 1)">' 
												: '<input type = "button" value = "Aktif" onclick = "nonaktifkan('.$v['id'].', 2)">';
				?>
				<tr>
					<td><?= $v['nama'] ?></td>
					<td><?= $v['telp'] ?></td>
					<td><?= $v['email'] ?></td>
					<td><?= $v['gender'] ?></td>
					<td><?= $v['pekerjaan'] ?></td>
					<td><input type = "button" value = "Hapus" onclick = "hapus(<?= $v['id']?>)">
					<a href = "<?= base_url()?>index.php/user/edit_user/<?= $v['id']?>"><input type = "button" value = "Edit"></a>
					<?= $status ?>
					</td>
				</tr>						
				<?php
			}
		?>
	</tbody>
</table>
 
<script type="text/javascript" src="<?= base_url()?>assets/datatables.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
   $('#cek').DataTable();
} );

var server = '<?php echo base_url(); ?>';
function hapus(id)
{
	var r = confirm('Yakin?');
	if(r)
	{
		window.location.replace(server + 'index.php/user/hapus_user/'+id);
		//alert(server);
	}
}

function nonaktifkan(id, kode)
{
	// 1 = non aktifkan, 2 = aktifkan;
	var ket = kode == 1 ? 'menonaktifkan' : 'mengaktifkan ';
	
	var r = confirm('Yakin akan '+ket+' data ini?');
	if(r)
	{
		window.location.replace(server + 'index.php/user/ubah_status/'+id+'/'+kode);
		//alert(server);
	}
}
</script>
</body>
</html>
