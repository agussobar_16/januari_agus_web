<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rt_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	function getDatanya()
	{
		$rw = $this->_getRw();
		$rt = $this->_getRt();
		$data = array();

		foreach($rw as $val_rw)
		{
			$data[$val_rw['id_rw']]= $val_rw;
			
			foreach($rt[$val_rw['id_rw']] as $val)
			{
				$data[$val_rw['id_rw']]['rt'][] = $val;
			}
		}

		return $data;
	}

	private function _getRw()
	{
		$query = "SELECT rw.id_rw
				       , rw.nama_rw
				       , SUM(jumlah_kk) AS jumlah_kk
				       , SUM(jumlah_l) AS jumlah_l
				       , SUM(jumlah_p) AS jumlah_p
				       , SUM(jumlah_l) + SUM(jumlah_p) AS jumlah_tot
				FROM rw 
				INNER JOIN rt
				ON rw.id_rw = rt.id_rw
				GROUP BY rw.id_rw , rw.nama_rw 
				ORDER BY id_rw";
		$get = $this->db->query($query);
		return $get->result_array();
	}

	private function _getRt()
	{	
		$data_return = array();
		$query = $this->db->get('rt');

		foreach($query->result_array() as $val)
		{
			$data_return[$val['id_rw']][] = $val;
		}

		return $data_return;
	}

	
}
